import { createReducer } from "redux-act";
import * as actions from "./houseData.actions";


const initialState = {
	houseList: [],
	listLoadedStatus: false,
	templateNumber: 1
};

const reducer = {
	[actions.setHoseList]: (state, houseList) => ({
		...state,
		houseList,
		listLoadedStatus: true
	}),

	[actions.setTemplateNumber]: (state, templateNumber) => ({
		...state,
		templateNumber
	}),

};


export default createReducer(reducer, initialState);
