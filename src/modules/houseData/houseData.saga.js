import api from "../../api";

import { loader } from "../../components/Loader/Loader";
import * as  houseActions from "./houseData.actions";
import { takeEvery, put, } from "redux-saga/effects";


function* fetchHouseList() {
	try {
		loader.show();

		const result = yield api.auth.getListOfHouse();

		yield put(houseActions.setHoseList(result.data.data));

		loader.hide();
	} catch (e) {
		loader.hide();
		console.log(e);
	}

	loader.hide();
}


export function* watchFetchUser() {
	yield takeEvery(houseActions.getHouseListSaga, fetchHouseList);
}


