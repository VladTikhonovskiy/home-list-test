import createSagaMiddleware from 'redux-saga';
import { compose, createStore, combineReducers, applyMiddleware } from "redux";

import houseData from "./houseData/houseData.reducer";
import { watchFetchUser } from "./houseData/houseData.saga";


const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({ houseData });

const store = createStore(rootReducer, undefined, compose(
	applyMiddleware(sagaMiddleware),
	window.devToolsExtension ? window.devToolsExtension() : (f) => f
));

sagaMiddleware.run(watchFetchUser);


export default store;
