import React, { PureComponent } from 'react';
import {  withRouter, Switch, Route } from "react-router-dom";
import { LoaderContainer } from "./components/Loader/Loader";

import HomePage from './container/HomePage/HomePage';
import HomePageWithId from './container/HomePageWithId/HomePageWithId';

import './App.scss';
import routes from "./constants/routes";


class App extends PureComponent {
	render() {
		return (
			<>
				<LoaderContainer />
				<Switch>
					<Route
						exact
						path={routes.homePage}
						component={HomePage}
					/>
					<Route
						path={routes.homePageWithId}
						component={HomePageWithId}
					/>
				</Switch>
			</>
		);
	}
}

export default withRouter(App);
