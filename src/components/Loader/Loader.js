/* eslint-disable react/forbid-prop-types */
import React, { Component } from 'react';
import * as ee from 'event-emitter';

import componentClasses from "./Loader.scss";
class LoaderContainer1 extends Component {
	state = {
		visible: false
	};

	componentDidMount() {
		if (!window.LoaderEmitter) {
			window.LoaderEmitter = ee();
		}
		window.LoaderEmitter.on('show', this.onShow);
		window.LoaderEmitter.on('hide', this.onHide);
	}

	componentWillUnmount() {
		window.LoaderEmitter.off('show', this.onShow);
		window.LoaderEmitter.off('hide', this.onHide);
	}

	onShow = () => {
		this.setState({ visible: true });
	};

	onHide = () => {
		this.setState({ visible: false });
	};

	render() {
		if (!this.state.visible) {
			return null;
		}

		return (
			<div className={componentClasses.rootLoader}>
				<div className={componentClasses.loader} />
			</div>
		);
	}
}

class Loader1 {
	static getEmitter() {
		if (!window.LoaderEmitter) {
			return null;
		}

		return window.LoaderEmitter;
	}

	static show() {
		const em = Loader1.getEmitter();

		if (em) {
			em.emit('show');
		}
	}

	static hide() {
		const em = Loader1.getEmitter();

		if (em) {
			setTimeout(() => {
				em.emit('hide');
			}, 500);
		}
	}
}

export const LoaderContainer = (LoaderContainer1);
export const loader = Loader1;
