import React, { Component } from 'react';
import { object, bool, number } from "prop-types";

import ImageGallery from 'react-image-gallery';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import Paper from '@material-ui/core/Paper';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import history from "../../modules/history";
import classes from "./HouseWrapper.scss";
import "react-image-gallery/styles/css/image-gallery.css";


class HouseWrapper extends Component {
    static propTypes = {
    	house: object,
		templateNumber: number,
		withoutDetails: bool
    }

    onDetailsClick = () => {
		const { house: { id } } = this.props;

		history.push(`/home/${id}`);
	}

	get imageGallery() {
		const { house: {  images } } = this.props;
		return images.map((item) => {
			return { original: item };
		});
	}

	renderGallery = () => {
		const { house: { full_address, price }, withoutDetails, templateNumber } = this.props;

		const houseWrapperImgWrapper = withoutDetails ?
			classes.houseWrapperImgWrapperSingle : classes.houseWrapperImgWrapper;

		const imagesGallery = this.imageGallery;

		return (
    		<>
				{ templateNumber !== 1 &&
					<Typography className={classes.houseWrapperAddress} >{ full_address }</Typography>
				}
				<div className={houseWrapperImgWrapper} >
					{
						templateNumber === 3 &&
							<h2 className={classes.houseWrapperPriceInGalery}>$ { price }</h2>
					}
					<ImageGallery
						showFullscreenButton={false}
						showPlayButton={false}
						showThumbnails={false}
						autoPlay
						items={imagesGallery}
					/>
				</div>
			</>
		);
	}

    render() {
    	const { house: { full_address, price, area, description }, withoutDetails, templateNumber } = this.props;

    	return (
				<Paper className={classes.houseWrapper} style={withoutDetails ? { width: "90%" } : {}}>
					{ this.renderGallery() }
					<ExpansionPanel>
						<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
							<Typography >О доме</Typography>
						</ExpansionPanelSummary>
						<ExpansionPanelDetails>
							<Typography>
								{ description }
							</Typography>
						</ExpansionPanelDetails>
					</ExpansionPanel>

					{ templateNumber === 1 &&
						<Typography classes={{ root: classes.houseWrapperAddress }} >{ full_address }</Typography>
					}

					{ templateNumber !== 3 &&
						<h2>$ {price}</h2>
					}
					<Typography >{ area }</Typography>
					{
						!withoutDetails &&
							<Typography className={classes.houseWrapperLink} onClick={this.onDetailsClick}>Подробнее</Typography>
					}
				</Paper>
    	);
    }
}

export default HouseWrapper;
