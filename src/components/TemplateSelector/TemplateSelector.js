import React, { Component } from 'react';
import { func, number } from "prop-types";
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';


class TemplateSelector extends Component {
	static propTypes = {
		onTemplateChange: func,
		templateNumber: number,
	}

	handleChange = (even) => {
		const { onTemplateChange } = this.props;
		const { target: { value } } = even;
 		onTemplateChange(+(value));
	}

	render() {
		const { templateNumber } = this.props;
		return (
			<Select
				value={templateNumber}
				onChange={this.handleChange}
				name="template"
				input={<Input id="name-error" />}
			>
				<MenuItem value="1">Template 1</MenuItem>
				<MenuItem value="2">Template 2</MenuItem>
				<MenuItem value="3">Template 3</MenuItem>
			</Select>
		);
	}
}


export default TemplateSelector;
