import React, { Component } from 'react';
import { connect } from "react-redux";
import {  withRouter } from "react-router-dom";
import { array, bool, func, number } from "prop-types";
import Grid from '@material-ui/core/Grid';
import Fade from "react-reveal/Fade";
import { loader } from "../../components/Loader/Loader";
import * as houseActions from "../../modules/houseData/houseData.actions";

import HouseWrapper from "../../components/HouseWrapper/HouseWrapper";
import TemplateSelector from "../../components/TemplateSelector/TemplateSelector";

import classes from "./HomePage.scss";


class HomePage extends Component {
    static propTypes = {
    	getHouseListSaga: func,
		houseList: array,
    	listLoadedStatus: bool,
		setTemplateNumber: func,
		templateNumber: number
    }

    componentDidMount() {
    	const { getHouseListSaga, listLoadedStatus } = this.props;

    	if (!listLoadedStatus) {
			loader.show();
			getHouseListSaga();
		}
    }

    onTemplateNumberChange = (value) => {
    	const { setTemplateNumber } = this.props;

		setTemplateNumber(value);
	}

    render() {
    	const { houseList, templateNumber } = this.props;

    	return (
			<Grid className={classes.homePage} container>
				<div className={classes.homePageTemplateWrapper}>
					<TemplateSelector onTemplateChange={this.onTemplateNumberChange} templateNumber={templateNumber} />
				</div>
				<Grid item xs={12}>
					<Grid className={classes.homePageGrid} container justify="center" spacing={8}>
						<Fade  bottom big>
							<div className={"check"}>
						{ houseList.map((house) => (
								<HouseWrapper key={house.id} house={house} templateNumber={templateNumber} />
						))}
						</div>
						</Fade>
					</Grid>
				</Grid>
			</Grid>
    	);
    }
}
function mapStateToProps({ houseData }) {
	return {
		houseList: houseData.houseList,
		listLoadedStatus: houseData.listLoadedStatus,
		templateNumber: houseData.templateNumber
	};
}

export default withRouter(connect(mapStateToProps, { ...houseActions })(HomePage));
