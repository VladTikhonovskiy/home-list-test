import React, { Component } from 'react';
import { connect } from "react-redux";
import {  withRouter } from "react-router-dom";
import { array, func, bool, object, number } from "prop-types";
import Grid from '@material-ui/core/Grid';
import { loader } from "../../components/Loader/Loader";
import * as houseActions from "../../modules/houseData/houseData.actions";

import HouseWrapper from "../../components/HouseWrapper/HouseWrapper";


class HomePageWithId extends Component {
    static propTypes = {
    	getHouseListSaga: func,
    	houseList: array,
		listLoadedStatus: bool,
		match: object,
		templateNumber: number
    }

    componentDidMount() {
    	const { listLoadedStatus, getHouseListSaga } = this.props;

    	if (!listLoadedStatus) {
			loader.show();
			getHouseListSaga();
		}
    }

	 get houseIndex() {
    	const { houseList, match: { params: { id }  } } = this.props;

		 return houseList.findIndex((item) => {
			 return item.id == id;
		});
	}


    render() {
    	const { houseList, listLoadedStatus, templateNumber } = this.props;

    	return (
			<Grid container spacing={16}>
				<Grid item xs={12}>
					<Grid container justify="center" spacing={8}>
						{ (houseList.length && listLoadedStatus) &&
							<HouseWrapper
								house={houseList[this.houseIndex]}
								templateNumber={templateNumber}
								withoutDetails
							/>
						}
					</Grid>
				</Grid>
			</Grid>
    	);
    }
}
function mapStateToProps({ houseData }) {
	return {
		houseList: houseData.houseList,
		listLoadedStatus: houseData.listLoadedStatus,
		templateNumber: houseData.templateNumber
	};
}

export default withRouter(connect(mapStateToProps, { ...houseActions })(HomePageWithId));
