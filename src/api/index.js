import AuthApi from "./auth.api";

export default {
	auth: new AuthApi()
};
