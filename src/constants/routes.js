export default {
    homePage: "/",
    homePageWithId: "/home/:id"
};
